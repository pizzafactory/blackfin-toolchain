#!/bin/bash
#
# This is a hacky script to parse a rpm .spec file when rpm is not
# available.  Basically this is used to build OS X toolchains from
# the .src.rpms ...
#

err() { echo "$*" 1>&2; exit 1; }
tac() { sed -n '1!G;h;$p'; }
cp() {
	if [ "$1" = "-a" ] ; then
		shift
		`which cp` -RPp "$@"
	else
		`which cp` "$@"
	fi
}
spec=${1:-`echo *.spec 2>/dev/null`}
[ -e "${spec}" ] || err "need a spec file"
shift

export RPM_BUILD_ROOT=${spec%/*}/rpm-build
case ${RPM_BUILD_ROOT} in
	./*) RPM_BUILD_ROOT=${PWD}/${RPM_BUILD_ROOT#./};;
	/*)  ;;
	*)   RPM_BUILD_ROOT=${PWD}/rpm-build;;
esac

steps="prep clean build install files"
order=$(grep `printf -- '-e ^%%%s$ ' ${steps}` ${spec} | sed s:%::g)
filter_spec() {
	sed \
		-e 's:%{:${:g' \
		-e 's:%setup\>:do_setup:' \
		-e 's:%patch:do_patch __:' \
		-e '/%if/{s:^%if :if test ":;s:$:" != 0; then:}' \
		-e 's:%else:else:' \
		-e 's:%endif:fi:' \
		-e '/%define/{s:^%define :\t:;s: :=":;s:$:":;}'
}
make_script() {
	(
	echo '#/bin/bash'
	echo '. ../.spec.env.sh'
	# then the actual step
	sed -n -e "/^%${1}$/,/^%${last}$/p" ${spec}
	) | filter_spec | \
		grep -v \
			-e "^%${1}$" \
			-e "^%${last}$" \
		> .spec.$1.sh
	chmod a+rx .spec.$1.sh
	last=$1
}
echo "### Ripping apart ${spec}"
for step in `printf '%s\n' "${order}" | tac` ; do
	make_script ${step}
done

# generate the shared spec env
(
# the global defines
echo 'nil='
sed -n -e '1,/^Name:/p' ${spec} | grep -v '^Name:'
# the package settings
awk '$0 ~ /^[^#]*:/ { system("printf " $1 " | tr [:upper:] [:lower:]"); $1 = ""; print }' ${spec} \
	| sed -n -e '/^[[:alnum:]]*:/{s|^\([^:]*\):[[:space:]]*\(.*\)|\1="\2"|;p}'
) | filter_spec > .spec.env.sh

# we need a few vars ourselves
. ./.spec.env.sh

dov() { echo "$@"; "$@"; }
do_setup() {
	local a b c n="${name}-${version}" t
	echo do_setup: "$@"
	while [ -n "$1" ] ; do
		case $1 in
		-a) shift; eval a=\"${a} \${source$1}\";;
		-b) shift; eval b=\"${b} \${source$1}\";;
		-c) c=1; shift; n=$1;;
		-q) :;;
		*)  err do_setup: unknown opt $1;;
		esac
		shift
	done

	dov cd ..
	dov rm -rf "${n}"
	if [ -n "${c}" ] ; then
		dov mkdir "${n}" || err
		dov cd "${n}" || err
	fi
	dov tar xf ../${source}
	dov cd ..

	if [ -n "${b}" ] ; then
		for t in ${b} ; do
			dov tar xf ${t}
		done
	fi
	dov cd "${n}" || err
	if [ -n "${a}" ] ; then
		for t in ${a} ; do
			dov tar xf ../${t}
		done
	fi
}
# the -P option is not supported
do_patch() {
	local p
	eval p=\"\${patch${1#__}}\"
	shift

	local x has_p=false has_f=false has_s=false
	for x in "$@" ; do
		case $x in
			-p*) has_p=true ;;
			-f)  has_f=true ;;
			-s)  has_s=true ;;
		esac
	done
	${has_p} || set -- "$@" -p0
	${has_f} || set -- "$@" -f
	${has_s} || set -- "$@" -s
	echo "do_patch: $patch $*"
	patch "$@" < "../$p" || err
}

if [ $# -eq 0 ] ; then
	set -- ${steps%files}
	echo "### cleaning build dirs"
	rm -rf /opt/uClinux "${RPM_BUILD_ROOT}" ${name}-${version}
fi
mkdir -p ${RPM_BUILD_ROOT} ${name}-${version} || err
cd "${name}-${version}" || err
for step in "$@" ; do
	sh=".spec.${step}.sh"
	echo "### Running ${step}"
	(. ../$sh) || err
	# in case the dir is recreated
	cd .
done

case " $* " in *" install "*)
	find "${RPM_BUILD_ROOT}" -perm +1 -type f -exec file {} + | \
	while read line ; do
		case ${line} in
		*"Mach-O executable"*) strip "${line%%:*}" ;;
		esac
	done
	osn=`sw_vers -productName | sed -e 's:Mac ::' -e 's: :_:g'`
	osv=`sw_vers -productVersion`
	tar cf - -C "${RPM_BUILD_ROOT}" ./ | bzip2 > ../${name}-${version}-${release}.${osn}-${osv}.`arch`.tar.bz2
;; esac
