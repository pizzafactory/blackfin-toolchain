/*******************************************************************************
 *  Copyright (c) 2009 Analog Devices, Inc.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *     Analog Devices, Inc. - Initial implementation
 *******************************************************************************/
package com.analog.gnu.debug;

/**
 * @author odcohen
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public interface IDSPDebugUIConstants
{
	// Default paths
	public static final String ADI_DSP = "ADI_DSP";
	public static final String LDR_RELATIVE_PATH = "/TS/ldr/";


	// Display Strings
	public static final String LAUNCH_MODE_FILE = "<Launching Executable>";

	// Action groups
	public static final String ACTION_GROUP_RUN = "com.adi.dsp.debug.ui.actions.run";


	// Menus
	public static final String EMPTY_PIPE_GROUP = "emptyPipeGroup"; //$NON-NLS-1$

	public static final String PIPE_GROUP = "pipeGroup"; //$NON-NLS-1$

	public static final String PIPE_PROPERTIES_GROUP = "pipePropertiesGroup"; //$NON-NLS-1$

	public static final String EMPTY_MEMORY_GROUP = "emptyMemoryGroup"; //$NON-NLS-1$

	public static final String MEMORY_GROUP = "memoryGroup"; //$NON-NLS-1$

	public static final String EMPTY_REGISTERS_GROUP = "emptyRegistersGroup"; //$NON-NLS-1$

	public static final String REGISTERS_GROUP = "registersGroup"; //$NON-NLS-1$

	public static final String EMPTY_REGISTERS_MODE_GROUP = "emptyRegistersModeGroup"; //$NON-NLS-1$

	public static final String REGISTERS_MODE_GROUP = "registersModeGroup"; //$NON-NLS-1$
}


