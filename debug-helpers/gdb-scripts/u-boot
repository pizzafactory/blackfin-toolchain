#
# Provide U-Boot-style functions in gdb
#

define uboot_jtag_load
	set remotetimeout 300
	load init.elf
	continue
	load u-boot
	call memset(&_bss_vma, 0, &_bss_len)
	continue
end


#
# U-Boot style memory display functions
#

define md
	if $argc == 2
		x/64x$arg0 $arg1
	else
		if $argc == 3
			x/$arg2x$arg0 $arg1
		else
			help md
		end
	end
end
document md
Usage: md <unit type> <address> [count=64]
Display [count] <unit type> starting at <address>.
end

define _md
	# dummy func for "document" to work
end
document _md
Usage: md[cbwl] <address> [count=64]
Display [count] <chars|bytes|words|longs> starting at <address>.
end

define mdb
	if $argc == 1
		md b $arg0
	else
		if $argc == 2
			md b $arg0 $arg1
		else
			help _md
		end
	end
end
document mdb
Use 'help _md'
end
define mdw
	if $argc == 1
		md h $arg0
	else
		if $argc == 2
			md h $arg0 $arg1
		else
			help _md
		end
	end
end
document mdw
Use 'help _md'
end
define mdl
	if $argc == 1
		md w $arg0
	else
		if $argc == 2
			md w $arg0 $arg1
		else
			help _md
		end
	end
end
document mdl
Use 'help _md'
end
define mdq
	if $argc == 1
		md g $arg0
	else
		if $argc == 2
			md g $arg0 $arg1
		else
			help _md
		end
	end
end
document mdq
Use 'help _md'
end
define mdc
	if $argc == 1
		md c $arg0
	else
		if $argc == 2
			md c $arg0 $arg1
		else
			help _md
		end
	end
end
document mdc
Use 'help _md'
end


#
# U-Boot style memory modify functions
#

define _mw
	set $$addr  = $arg0
	set $$val   = $arg1
	set $$count = $arg2
	while $$count-- > 0
		set *$$addr = $$val
		set $$addr += 1
	end
	dont-repeat
end
document _mw
Usage: mw[bwl] <address> <value> <count>
Set <count> <bytes|words|longs> at <address> to <value>.
end

define mwl
	if $argc == 3
		set $$addr = (unsigned long *)$arg0
		_mw $$addr $arg1 $arg2
	else
		help _mw
	end
end
document mwl
Use 'help _mw'
end
define mww
	if $argc == 3
		set $$addr = (unsigned short *)$arg0
		_mw $$addr $arg1 $arg2
	else
		help _mw
	end
end
document mww
Use 'help _mw'
end
define mwb
	if $argc == 3
		set $$addr = (unsigned char *)$arg0
		_mw $$addr $arg1 $arg2
	else
		help _mw
	end
end
document mwb
Use 'help _mw'
end


#
# U-Boot style memory compare functions
#

define _cmp
	set $$base  = (unsigned long)$arg0
	set $$addr1 = $arg0
	set $$addr2 = $arg1
	set $$count = $arg2
	while $$count-- > 0
		if (*$$addr1 != *$$addr2)
			printf "Data mismatch at %#x units (@%#x != @%#x)\n", $arg2, $$addr1, $$addr2
			set $$count = -100
		end
		set $$addr1 += 1
		set $$addr2 += 1
	end
	if $$count == -1
		printf "Data matches for %#x units\n", $arg2
	end
	dont-repeat
end
document _cmp
Usage: cmp[bwl] <address> <address> <count>
Compare <count> <bytes|words|longs> between <address> and <address>.
end

define cmpb
	if $argc == 3
		set $$addr1 = (unsigned char *)$arg0
		set $$addr2 = (unsigned char *)$arg1
		_cmp $$addr1 $$addr2 $arg2
	else
		help _cmp
	end
end
document cmpb
Use 'help _cmp'
end
define cmpw
	if $argc == 3
		set $$addr1 = (unsigned short *)$arg0
		set $$addr2 = (unsigned short *)$arg1
		_cmp $$addr1 $$addr2 $arg2
	else
		help _cmp
	end
end
document cmpw
Use 'help _cmp'
end
define cmpl
	if $argc == 3
		set $$addr1 = (unsigned long *)$arg0
		set $$addr2 = (unsigned long *)$arg1
		_cmp $$addr1 $$addr2 $arg2
	else
		help _cmp
	end
end
document cmpl
Use 'help _cmp'
end


#
# U-Boot style memory copy functions
#

define _cp
	set $$src = $arg0
	set $$dst = $arg1
	set $$count = $arg2
	while $$count-- > 0
		set *$$dst = *$$src
		set $$src += 1
		set $$dst += 1
	end
	printf "Copied %#x units from %#x to %#x\n", $arg2, $arg0, $arg1
	dont-repeat
end
document _cp
Usage: cp[bwl] <src> <dst> <count>
Copy <count> <bytes|words|longs> from <src> to <dst>.
end

define cpb
	if $argc == 3
		set $$addr1 = (unsigned char *)$arg0
		set $$addr2 = (unsigned char *)$arg1
		_cp $$addr1 $$addr2 $arg2
	else
		help _cp
	end
end
document cpb
Use 'help _cp'
end
define cpw
	if $argc == 3
		set $$addr1 = (unsigned short *)$arg0
		set $$addr2 = (unsigned short *)$arg1
		_cp $$addr1 $$addr2 $arg2
	else
		help _cp
	end
end
document cpw
Use 'help _cp'
end
define cpl
	if $argc == 3
		set $$addr1 = (unsigned long *)$arg0
		set $$addr2 = (unsigned long *)$arg1
		_cp $$addr1 $$addr2 $arg2
	else
		help _cp
	end
end
document cpl
Use 'help _cp'
end
